﻿
namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public int Id { get; set; }

        public int NumeroPedido { get; set; }

        public DateTime DataPedido { get; set; }

        public Vendedor vendedor { get; set; }

        public StatusVenda StatusVenda { get; set; }



    }
}
