﻿using tech_test_payment_api.Models;

namespace tech_test_payment_api.Repository
{
    public class StatusRepositorio
    {
        private static List<StatusVenda> status;

        public static void GerarStatus()
        {

            status = new List<StatusVenda>();

            status.Add(new StatusVenda
            {
                Id = 1,
                Descricao = "Aguardando Pagamento",

            });

            status.Add(new StatusVenda
            {
                Id = 2,
                Descricao = "Cancelada",

            });

            status.Add(new StatusVenda
            {
                Id = 3,
                Descricao = "Pagamento Aprovado",

            });

            status.Add(new StatusVenda
            {
                Id = 4,
                Descricao = "Enviado para Transportadora",

            });

            status.Add(new StatusVenda
            {
                Id = 5,
                Descricao = "Entregue",

            });

        }
        public static List<StatusVenda> Status
        {
            get
            {
                if(status == null)
                {
                    GerarStatus();
                }
                return status;

            }
           
        }
    }

    

}


