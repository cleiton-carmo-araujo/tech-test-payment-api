﻿using tech_test_payment_api.Data;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Repository
{
    public class VendaRepositorio
    {
        private readonly VendaContext _context;
     
        public VendaRepositorio(VendaContext vendaContext)
        {
            _context = vendaContext;
        }

        public Venda Adicionar(Venda venda)
        {
            venda.DataPedido = DateTime.Now;
            venda.StatusVenda = StatusRepositorio.Status.FirstOrDefault(l => l.Id == 1);
            
            _context.Vendas.Add(venda);
            _context.SaveChanges();

            return venda;

        }

        public Venda ListarPorId(int id)
        {
            return _context.Vendas.FirstOrDefault(b => b.Id == id);

        }

    }

    

}


