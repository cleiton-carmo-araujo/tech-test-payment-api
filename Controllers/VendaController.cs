﻿using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using tech_test_payment_api.Repository;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendaRepositorio _vendaRepositorio;

        public VendaController(VendaRepositorio vendaRepositorio)
        {
            _vendaRepositorio = vendaRepositorio;
        }

        [HttpPost()]
        public IActionResult CriarVenda(Venda venda)
        {
  
              _vendaRepositorio.Adicionar(venda);
                    
              return CreatedAtAction(nameof(ObterVendaId), new { id = venda.Id }, venda); ;
                             
        }
            

        [HttpGet("{id}")]
        public IActionResult ObterVendaId(int id)
        {
            Venda venda = _vendaRepositorio.ListarPorId(id);
                

            if (venda != null)
                return Ok(venda);

            else
                return NotFound();

        }

      

        //[HttpPut("{id}")]
        //public IActionResult AtualizarStatusVenda(Venda venda)
        //{
        //    return Ok();
        //}
    }
}
